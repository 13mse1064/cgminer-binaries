/*
 * Copyright 2012-2013 Andrew Smith
 * Copyright 2013 Con Kolivas <kernel@kolivas.org>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option)
 * any later version.  See COPYING for more details.
 *
 * To normally build - do something like:
 *  gcc -I/usr/include/libusb-1.0 -g -W -Wall usbfail.c -o usbfail -lusb-1.0 -lpthread
 *
 * However to build against your own compiled libusb, something like:
 *  gcc -Ilibusb/libusb-1.0.16-rc10/libusb/ -g -W -Wall usbfail.c -o usbfail \
 *	libusb/libusb-1.0.16-rc10/libusb/.libs/libusb-1.0.a -lpthread -ludev
 * You may also have to add "-lrt" on the end if you get a link error requiring it
 */

#include <stdio.h>
#include <ctype.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>
#include <libusb.h>

#define EPI(x) (LIBUSB_ENDPOINT_IN | (unsigned char)(x))
#define EPO(x) (LIBUSB_ENDPOINT_OUT | (unsigned char)(x))

#define DEFAULT_EP_IN 0
#define DEFAULT_EP_OUT 1

// For 0x10c4:0xea60 USB cp210x chip - AMU
#define CP210X_TYPE_OUT 0x41

#define CP210X_REQUEST_IFC_ENABLE 0x00
#define CP210X_REQUEST_DATA 0x07
#define CP210X_REQUEST_BAUD 0x1e

#define CP210X_VALUE_UART_ENABLE 0x0001
#define CP210X_VALUE_DATA 0x0303
#define CP210X_DATA_BAUD 0x0001c200


// For 0x067b:0x2303 Prolific PL2303 - ICA
#define PL2303_CTRL_DTR 0x01
#define PL2303_CTRL_RTS 0x02
//
#define PL2303_CTRL_OUT 0x21
#define PL2303_VENDOR_OUT 0x40

#define PL2303_REQUEST_CTRL 0x22
#define PL2303_REQUEST_LINE 0x20
#define PL2303_REQUEST_VENDOR 0x01

#define PL2303_REPLY_CTRL 0x21

#define PL2303_VALUE_CTRL (PL2303_CTRL_DTR | PL2303_CTRL_RTS)
#define PL2303_VALUE_LINE 0
#define PL2303_VALUE_LINE0 0x0001c200
#define PL2303_VALUE_LINE1 0x080000
#define PL2303_VALUE_LINE_SIZE 7
#define PL2303_VALUE_VENDOR 0

#ifdef ARRAY_SIZE
#undef ARRAY_SIZE
#endif

#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof((arr)[0]))

static int id = 0;

static int in = 0;

static bool lock = false;

static pthread_mutex_t usb_lock;

struct usb_dev {
	int id;
	struct libusb_device_handle *handle;
	struct usb_find_devices *found;
};

struct usb_endpoints {
	uint8_t att;
	uint16_t size;
	unsigned char ep;
	bool found;
};

static struct usb_endpoints ica_eps[] = {
	{ LIBUSB_TRANSFER_TYPE_BULK,	64,	EPI(3), 0 },
	{ LIBUSB_TRANSFER_TYPE_BULK,	64,	EPO(2), 0 }
};

static struct usb_endpoints amu_eps[] = {
	{ LIBUSB_TRANSFER_TYPE_BULK,	64,	EPI(1), 0 },
	{ LIBUSB_TRANSFER_TYPE_BULK,	64,	EPO(1), 0 }
};

struct usb_find_devices {
	int drv;
	const char *name;
	uint16_t idVendor;
	uint16_t idProduct;
	int kernel;
	int config;
	int interface;
	unsigned int timeout;
	int epcount;
	struct usb_endpoints *eps;
};

static struct usb_find_devices find_dev[] = {
	{
		.drv = 1,
		.name = "ICA",
		.idVendor = 0x067b,
		.idProduct = 0x2303,
		.kernel = 0,
		.config = 1,
		.interface = 0,
		.timeout = 100,
		.epcount = ARRAY_SIZE(ica_eps),
		.eps = ica_eps },
	{
		.drv = 1,
		.name = "AMU",
		.idVendor = 0x10c4,
		.idProduct = 0xea60,
		.kernel = 0,
		.config = 1,
		.interface = 0,
		.timeout = 100,
		.epcount = ARRAY_SIZE(amu_eps),
		.eps = amu_eps },
	{
		.drv = 0,
		.name = NULL,
		.idVendor = 0,
		.idProduct = 0,
		.kernel = 0,
		.config = 0,
		.interface = 0,
		.timeout = 0,
		.epcount = 0,
		.eps = NULL }
};

#define SECTOMS(s) ((int)((s) * 1000))

#define LOG(fmt, ...) do { \
		char tmp42[512]; \
		snprintf(tmp42, sizeof(tmp42), fmt "\n", ##__VA_ARGS__); \
		fprintf(stderr, tmp42); \
		fflush(stderr); \
} while (0)

#define DLOG(dev_, fmt, ...) do { \
		char tmp42[512]; \
		snprintf(tmp42, sizeof(tmp42), "%d %s: " fmt "\n", dev_->id, dev_->found->name, ##__VA_ARGS__); \
		fprintf(stderr, tmp42); \
		fflush(stderr); \
} while (0)

#define QUIT(fmt, ...) do { \
		char tmp42[512]; \
		snprintf(tmp42, sizeof(tmp42), fmt "\n", ##__VA_ARGS__); \
		fprintf(stderr, tmp42); \
		fflush(stderr); \
		exit(1); \
} while (0)

#define TDIFF(end, start) ((end)->tv_sec - (start)->tv_sec + ((end)->tv_usec - (start)->tv_usec) / 1000000.0)

#define LOCK() if (lock) { \
	if (pthread_mutex_lock(&usb_lock)) \
		QUIT("Mutex lock failed"); \
}

#define UNLOCK() if (lock) { \
	if (pthread_mutex_unlock(&usb_lock)) \
		QUIT("Mutex unlock failed"); \
}

#define USB_MAX_READ 8192
#define USB_RETRY_MAX 5

int usb_bulk_transfer(struct usb_dev *dev, unsigned char ep, unsigned char *data, int length, int *transferred, unsigned int timeout)
{
	int err, errn;

	LOCK();
	err = libusb_bulk_transfer(dev->handle, ep, data, length, transferred, timeout);
	errn = errno;
	UNLOCK();
	if (err < 0)
		DLOG(dev, "amt=%d err=%d ern=%d", *transferred, err, errn);

	return err;
}

int usb_read(struct usb_dev *dev, int ep, char *buf, size_t bufsiz, int *processed, unsigned int timeout)
{
	struct timeval read_start, tv_finish;
	unsigned int initial_timeout;
	double max, done;
	int bufleft, err, got, tot;
	unsigned char *ptr, usbbuf[256];
	size_t usbbufread;

	tot = 0;
	bufleft = bufsiz;
	ptr = usbbuf;

	err = LIBUSB_SUCCESS;
	initial_timeout = timeout;
	max = ((double)timeout) / 1000.0;
	gettimeofday(&read_start, NULL);
	while (bufleft > 0) {
		usbbufread = bufleft;
		got = 0;

		err = usb_bulk_transfer(dev, dev->found->eps[ep].ep, ptr, usbbufread, &got, timeout);
		gettimeofday(&tv_finish, NULL);
		ptr[got] = '\0';

		tot += got;

		if (err)
			break;

		ptr += got;
		bufleft -= got;

		done = TDIFF(&tv_finish, &read_start);
		// N.B. this is: return LIBUSB_SUCCESS with whatever size has already been read
		if (done >= max)
			break;
		timeout = initial_timeout - (done * 1000);
		if (!timeout)
			break;
	}

	*processed = tot;
	memcpy((char *)buf, (const char *)usbbuf, (tot < (int)bufsiz) ? tot + 1 : (int)bufsiz);

	return err;
}

int usb_write(struct usb_dev *dev, int ep, char *buf, size_t bufsiz, int *processed, unsigned int timeout)
{
	struct timeval read_start, tv_finish;
	unsigned int initial_timeout;
	double max, done;
	int err, sent, tot;

	*processed = 0;

	tot = 0;
	err = LIBUSB_SUCCESS;
	initial_timeout = timeout;
	max = ((double)timeout) / 1000.0;
	gettimeofday(&read_start, NULL);
	while (bufsiz > 0) {
		sent = 0;
		err = usb_bulk_transfer(dev, dev->found->eps[ep].ep, (unsigned char *)buf, bufsiz, &sent, timeout);
		gettimeofday(&tv_finish, NULL);

		tot += sent;

		if (err)
			break;

		buf += sent;
		bufsiz -= sent;

		done = TDIFF(&tv_finish, &read_start);
		// N.B. this is: return LIBUSB_SUCCESS with whatever size was written
		if (done >= max)
			break;
		timeout = initial_timeout - (done * 1000);
		if (!timeout)
			break;
	}

	*processed = tot;

	return err;
}

int usb_transfer(struct usb_dev *dev, uint8_t request_type, uint8_t bRequest, uint16_t wValue, uint16_t wIndex, uint32_t *data, int siz, unsigned int timeout)
{
	uint32_t *buf = NULL;
	int err, i, bufsiz;

	if (siz > 0) {
		bufsiz = siz - 1;
		bufsiz >>= 2;
		bufsiz++;
		buf = malloc(bufsiz << 2);
		if (!buf)
			QUIT("Failed to malloc in usb_transfer");
		for (i = 0; i < bufsiz; i++)
			buf[i] = htole32(data[i]);
	}

	LOCK();
	err = libusb_control_transfer(dev->handle, request_type,
		bRequest, wValue, wIndex, (unsigned char *)buf, (uint16_t)siz, timeout);
	UNLOCK();

	free(buf);

	return err;
}

int usb_transfer_read(struct usb_dev *dev, uint8_t request_type, uint8_t bRequest, uint16_t wValue, uint16_t wIndex, char *buf, int bufsiz, int *amount, unsigned int timeout)
{
	int err;

	*amount = 0;

	LOCK();
	err = libusb_control_transfer(dev->handle, request_type, bRequest, wValue, wIndex, (unsigned char *)buf, (uint16_t)bufsiz, timeout);
	UNLOCK();

	if (err > 0) {
		*amount = err;
		err = 0;
	}

	return err;
}

void hex2bin(unsigned char *p, const char *hexstr, size_t len)
{
	while (*hexstr && len) {
		char hex_byte[4];
		unsigned int v;

		if (!hexstr[1])
			QUIT("hex2bin str truncated");

		memset(hex_byte, 0, 4);
		hex_byte[0] = hexstr[0];
		hex_byte[1] = hexstr[1];

		if (sscanf(hex_byte, "%x", &v) != 1)
			QUIT("hex2bin sscanf '%s' failed", hex_byte);

		*p = (unsigned char) v;

		p++;
		hexstr += 2;
		len--;
	}
}

void nmsleep(unsigned int msecs)
{
	struct timespec twait, tleft;
	int ret;
	ldiv_t d;

	d = ldiv(msecs, 1000);
	tleft.tv_sec = d.quot;
	tleft.tv_nsec = d.rem * 1000000;
	do {
		twait.tv_sec = tleft.tv_sec;
		twait.tv_nsec = tleft.tv_nsec;
		ret = nanosleep(&twait, &tleft);
	} while (ret == -1 && errno == EINTR);
}

static void test1(struct usb_dev *dev, char *which, const char *test_ob, const uint32_t test_nonce_val, int delay, bool expect0)
{
	unsigned char ob_bin[64], nonce_bin[4];
	struct timeval tv_stt, tv_fin;
	int err, amount;

	hex2bin(ob_bin, test_ob, sizeof(ob_bin));

	err = usb_write(dev, DEFAULT_EP_OUT, (char *)ob_bin, sizeof(ob_bin), &amount, 300);

	nonce_bin[0] = 0;
	nonce_bin[1] = 0;
	nonce_bin[2] = 0;
	nonce_bin[3] = 0;

	if (delay)
		nmsleep(delay);

	gettimeofday(&tv_stt, NULL);

	int red = 0;
	while (err == 0 && red < 4) {
		err = usb_read(dev, DEFAULT_EP_IN, (char *)(nonce_bin + red), 4 - red, &amount, 500);
		red += amount;
		DLOG(dev, "%s read a reply err=%d of %d bytes - total now %d",
			which, err, amount, red);
	}

	gettimeofday(&tv_fin, NULL);

	if (expect0)
		DLOG(dev, "%s Expecting a timeout answer of 0x00000000 ...\n"
			  "%8s%s answer=0x%02x%02x%02x%02x (hash=0x%08x) elapsed=%.6fs\n",
			which, "", which, nonce_bin[0], nonce_bin[1], nonce_bin[2], nonce_bin[3],
			test_nonce_val, TDIFF(&tv_fin, &tv_stt));
	else
		DLOG(dev, "%s answer=0x%02x%02x%02x%02x (hash=0x%08x) elapsed=%.6fs\n",
			which, nonce_bin[0], nonce_bin[1], nonce_bin[2], nonce_bin[3],
			test_nonce_val, TDIFF(&tv_fin, &tv_stt));
}

static void test(struct usb_dev *dev)
{
	const char golden_ob[] =
		"4679ba4ec99876bf4bfe086082b40025"
		"4df6c356451471139a3afa71e48f544a"
		"00000000000000000000000000000000"
		"0000000087320b1a1426674f2fa722ce";
	const uint32_t golden_nonce_val = 0x000187a2;

	const char test_ob[] =
		"78fe95191a3998a5c7e0b981801f7d65"
		"c018c6e1aec3d881cf5e60cefd62dd20"
		"00000000000000000000000000000000"
		"000000006889001ac299f251dc1ec555";
	const uint32_t test_nonce_val = 0xa49711ad;

	unsigned char nonce_bin[4];
	int err, amount;

	in++;

	DLOG(dev, "Init success - testing ...\n");

	if (strcmp(dev->found->name, "ICA") == 0) {
		// Set Data Control
		err = usb_transfer(dev, PL2303_CTRL_OUT, PL2303_REQUEST_CTRL, PL2303_VALUE_CTRL,
				dev->found->interface, NULL, 0, 200);
		if (err < 0)
			DLOG(dev, "Set data control failed err=%d", err);

		// Set Line Control
		uint32_t ica_data[2] = { PL2303_VALUE_LINE0, PL2303_VALUE_LINE1 };
		err = usb_transfer(dev, PL2303_CTRL_OUT, PL2303_REQUEST_LINE, PL2303_VALUE_LINE,
				dev->found->interface, &ica_data[0], PL2303_VALUE_LINE_SIZE, 200);
		if (err < 0)
			DLOG(dev, "Set line control failed err=%d", err);

		// Vendor
		err = usb_transfer(dev, PL2303_VENDOR_OUT, PL2303_REQUEST_VENDOR, PL2303_VALUE_VENDOR,
				dev->found->interface, NULL, 0, 200);
		if (err < 0)
			DLOG(dev, "Set vendor failed err=%d", err);

	}

	if (strcmp(dev->found->name, "AMU") == 0) {
		// Enable the UART
		err = usb_transfer(dev, CP210X_TYPE_OUT, CP210X_REQUEST_IFC_ENABLE,
				CP210X_VALUE_UART_ENABLE, dev->found->interface, NULL, 0, 200);
		if (err < 0)
			DLOG(dev, "Enable UART failed err=%d", err);

		// Set data control
		err = usb_transfer(dev, CP210X_TYPE_OUT, CP210X_REQUEST_DATA, CP210X_VALUE_DATA,
				dev->found->interface, NULL, 0, 200);
		if (err < 0)
			DLOG(dev, "Set data control failed err=%d", err);

		// Set the baud
		uint32_t data = CP210X_DATA_BAUD;
		err = usb_transfer(dev, CP210X_TYPE_OUT, CP210X_REQUEST_BAUD, 0,
				dev->found->interface, &data, sizeof(data), 200);
		if (err < 0)
			DLOG(dev, "Set baud failed err=%d", err);
	}

	DLOG(dev, "Flushing ...\n");

	nonce_bin[0] = 0;
	nonce_bin[1] = 0;
	nonce_bin[2] = 0;
	nonce_bin[3] = 0;

	err = 0;
	amount = 1;
	while (err == 0 && amount > 0) {
		err = usb_read(dev, DEFAULT_EP_IN, (char *)nonce_bin, 4, &amount, 30);
		DLOG(dev, "Flush reply err=%d %d bytes", err, amount);
	}

	test1(dev, "A", golden_ob, golden_nonce_val, 100, false);

	nmsleep(500);

	test1(dev, "B", test_ob, test_nonce_val, 0, true);

	in--;
}

static void *dotest(void *user)
{
	struct usb_dev *dev = (struct usb_dev *)user;

	pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);

	test(dev);

	return NULL;
}

void starttest(struct usb_dev *dev)
{
	pthread_t thr;

	if (pthread_create(&thr, NULL, dotest, dev))
		DLOG(dev, "Failed to start thread");
}

#define STRBUFLEN 1024

static void usb_init(struct libusb_device *udev, struct usb_dev *dev)
{
	struct libusb_device_descriptor *descriptor = NULL;
	struct libusb_config_descriptor *config = NULL;
	const struct libusb_interface_descriptor *idesc;
	const struct libusb_endpoint_descriptor *epdesc;
	unsigned char strbuf[STRBUFLEN+1];
	int err, i, j, k, cfg;

	DLOG(dev, "Processing bus:dev %d:%d",
		(int)libusb_get_bus_number(udev), (int)libusb_get_device_address(udev));

	descriptor = calloc(1, sizeof(*descriptor));
	if (!descriptor)
		QUIT("Failed to calloc descriptor");

	err = libusb_get_device_descriptor(udev, descriptor);
	if (err)
		QUIT("Failed to get descriptor, err %d", err);

	err = libusb_open(udev, &dev->handle);
	if (err) {
		switch (err) {
			case LIBUSB_ERROR_ACCESS:
				QUIT("Init open device failed, err %d, "
					"you dont have priviledge to access it", err);
#ifdef WIN32
			// Windows specific message
			case LIBUSB_ERROR_NOT_SUPPORTED:
				QUIT("Init, open device failed, err %d, "
					"you need to install a WinUSB driver", err);
#endif
			default:
				QUIT("USB init, open failed, err %d", err);
		}
	}

#ifndef WIN32
	if (libusb_kernel_driver_active(dev->handle, dev->found->kernel) == 1) {
		DLOG(dev, "Init, kernel attached ...");
		err = libusb_detach_kernel_driver(dev->handle, dev->found->kernel);
		if (err == 0)
			DLOG(dev, "Init, kernel detached successfully");
		else
			QUIT("Init, kernel detach failed, err %d in use?", err);
	}
#endif

	err = libusb_get_string_descriptor_ascii(dev->handle, descriptor->iManufacturer, strbuf, STRBUFLEN);
	if (err < 0)
		DLOG(dev, "Init, failed to get iManufacturer, err %d", err);
	else
		DLOG(dev, "Init, iManufacturer = '%s'", strbuf);

	err = libusb_get_string_descriptor_ascii(dev->handle, descriptor->iProduct, strbuf, STRBUFLEN);
	if (err < 0)
		DLOG(dev, "Init, failed to get iProduct, err %d", err);
	else
		DLOG(dev, "Init, iProduct = '%s'", strbuf);

	cfg = -1;
	err = libusb_get_configuration(dev->handle, &cfg);
	if (err)
		cfg = -1;

	// Try to set it if we can't read it or it's different
	if (cfg != dev->found->config) {
		err = libusb_set_configuration(dev->handle, dev->found->config);
		if (err) {
			switch(err) {
				case LIBUSB_ERROR_BUSY:
					QUIT("Init, set config %d in use", dev->found->config);
				default:
					QUIT("Init, failed to set config to %d, err %d", dev->found->config, err);
			}
		}
	}

	err = libusb_get_active_config_descriptor(udev, &config);
	if (err)
		QUIT("Init, failed to get config descriptor, err %d", err);

	if ((int)(config->bNumInterfaces) <= dev->found->interface)
		QUIT("Init bNumInterfaces (%d) <= interface (%d)",
			(int)(config->bNumInterfaces), dev->found->interface);

	for (i = 0; i < dev->found->epcount; i++)
		dev->found->eps[i].found = false;

	for (i = 0; i < config->interface[dev->found->interface].num_altsetting; i++) {
		idesc = &(config->interface[dev->found->interface].altsetting[i]);
		for (j = 0; j < (int)(idesc->bNumEndpoints); j++) {
			epdesc = &(idesc->endpoint[j]);
			for (k = 0; k < dev->found->epcount; k++) {
				if (!dev->found->eps[k].found) {
					if (epdesc->bmAttributes == dev->found->eps[k].att
					&&  epdesc->wMaxPacketSize >= dev->found->eps[k].size
					&&  epdesc->bEndpointAddress == dev->found->eps[k].ep) {
						dev->found->eps[k].found = true;
						//dev->found->wMaxPacketSize = epdesc->wMaxPacketSize;
						break;
					}
				}
			}
		}
	}

	for (i = 0; i < dev->found->epcount; i++) {
		if (dev->found->eps[i].found == false)
			QUIT("Init found == false");
	}

	err = libusb_claim_interface(dev->handle, dev->found->interface);
	if (err) {
		switch(err) {
			case LIBUSB_ERROR_BUSY:
				QUIT("Init, claim interface %d in use", dev->found->interface);
			default:
				QUIT("Init, claim interface %d failed, err %d", dev->found->interface, err);
		}
	}

	cfg = -1;
	err = libusb_get_configuration(dev->handle, &cfg);
	if (err)
		cfg = -1;
	if (cfg != dev->found->config)
		QUIT("Init, incorrect config (%d!=%d) after claim", cfg, dev->found->config);

	libusb_free_config_descriptor(config);

	id++;

	starttest(dev);
}

static void usb_check_device(struct libusb_device *dev, struct usb_find_devices *look)
{
	struct libusb_device_descriptor desc;
	struct usb_dev *usbdev;
	int err;

	err = libusb_get_device_descriptor(dev, &desc);
	if (err) {
		LOG("USB check device %d:%d failed to get descriptor, ignoring, err %d",
			(int)libusb_get_bus_number(dev), (int)libusb_get_device_address(dev), err);
		return;
	}

	if (desc.idVendor != look->idVendor || desc.idProduct != look->idProduct)
		return;

	usbdev = calloc(1, sizeof(*usbdev));
	usbdev->id = id;
	usbdev->handle = NULL;
	usbdev->found = look;

	usb_init(dev, usbdev);
}

static void usb_check(struct libusb_device *dev)
{
	int i;

	for (i = 0; find_dev[i].drv != 0; i++) {
		// limit it to 8 - more than enough
		if (id < 8)
			usb_check_device(dev, &(find_dev[i]));
	}
}

void usb_detect()
{
	libusb_device **list;
	ssize_t count, i;

	count = libusb_get_device_list(NULL, &list);
	if (count < 0)
		QUIT("USB scan devices: failed, err %d", (int)count);

	if (count == 0)
		QUIT("USB scan devices: found no devices");

	LOG("This test requires at least 2 AMUs and/or ICAs\n");
	LOG("If it locks up during flushing and does no tests then USB is screwed\n");
	LOG("With each device - test A should work or:");
	LOG("   the devices will go SICK in cgminer");
	LOG("   and Test B won't happen\n");
	LOG("Test B will produce one of:");
	LOG("a) elapsed approx 0.5seconds with a zero reply and a -7 error:");
	LOG("      USB is working correctly - except:");
	LOG("          if you get a non-zero answer, you must rerun the test");
	LOG("b) elapsed approx 8seconds with a correct reply:");
	LOG("      USB is screwed you'll get about 100MH/s per AMU/ICA");
	LOG("c) no reply after more than 15seconds:");
	LOG("      USB is screwed and AMU/ICA will go SICK\n");

	for (i = 0; i < count; i++)
		usb_check(list[i]);

	libusb_free_device_list(list, 1);
}

int main(int argc, char **argv)
{
	// parameter just has to start with an L
	if (argc > 1 &&
		(strncasecmp(argv[1], "lock", 1) == 0 ||
		 strncasecmp(argv[1], "-lock", 2) == 0 ||
		 strncasecmp(argv[1], "--lock", 3) == 0)) {

		lock = true;

		if (pthread_mutex_init(&usb_lock, NULL))
			QUIT("Failed to initialise usb_lock");
	}

	libusb_init(NULL);

	usb_detect();

	if (id == 0) {
		libusb_exit(NULL);

		QUIT("Failed to find any ICA or AMU devices");
	}

	LOG("Test will complete in ~17s");
	nmsleep(17000);

	LOG("\nAt exit, in=%d", in);
	if (in) {
		LOG("That's a failure ... should be in=0");
		if (lock)
			LOG("And even worse, locking didn't fix it");
	} else {
		LOG("Looks like USB worked correctly");
		if (lock)
			LOG("However you used locking which tries to work around the problem");
	}

	if (in == 0)
		libusb_exit(NULL);

	exit(0);
}
